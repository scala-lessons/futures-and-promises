
ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "futures-and-promises"
  )

val productionDependencies = Seq(
)

val testDependencies = Seq(
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test,
  "org.specs2" %% "specs2-mock" % "4.12.3" % Test
)

libraryDependencies ++= productionDependencies ++ testDependencies

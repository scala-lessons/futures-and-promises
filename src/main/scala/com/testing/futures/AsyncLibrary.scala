package com.testing.futures

import scala.concurrent.{ExecutionContext, Future}

object AsyncLibrary extends AsyncFixture {

  override def fm2(arg: Int)(implicit ec: ExecutionContext): Future[Int] = Future {
    arg * 2
  }

  override def fm3(arg: Double)(implicit ec: ExecutionContext): Future[Double] = Future {
    arg * 3
  }

  override def fm6(arg: Int)(implicit ec: ExecutionContext): Future[Double] = for {
    x1 <- fm2(arg)
    x2 <- fm3(x1)
  } yield {
    x2
  }

  override def fm6fm(arg: Int)(implicit ec: ExecutionContext): Future[Double] = fm2(arg).flatMap {
    fm3(_)
  }
}

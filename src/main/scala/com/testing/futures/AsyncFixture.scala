package com.testing.futures

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.Try

trait AsyncFixture {

  def fm2(arg: Int)(implicit ec: ExecutionContext): Future[Int]

  def fm3(arg: Double)(implicit ec: ExecutionContext): Future[Double]

  def fm6(arg: Int)(implicit ec: ExecutionContext): Future[Double]

  def fm6fm(arg: Int)(implicit ec: ExecutionContext): Future[Double]

  def print[A](value: A)(implicit ec: ExecutionContext): Future[Unit] = Future {
    println(value)
  }

  def printWithDelay[A](value: A, delay: FiniteDuration)(implicit ec: ExecutionContext): Future[Unit] = Future {
    Thread.sleep(delay.toMillis)
    println(value)
  }

  def printViaPromise[A](value: A, delay: FiniteDuration)(implicit ec: ExecutionContext): Future[Unit] = {
    val promise = Promise[Unit]()
    val result = promise.future
    if (value.isInstanceOf[Int]) {
      Thread.sleep(delay.toMillis)
      promise.complete(Try {
        println("This is integer")
      })
    } else {
      promise.complete(Try {
        println(value)
      })
    }
    result
  }

}



package com.testing.implicits

trait MyMathOps[A] {

  def plus(a: A, b: A): A

  def plusC(a: A)(implicit b: A): A

  def multiply(a: A, b: A): A

  def multiplyC(a: A)(implicit b: A): A

}

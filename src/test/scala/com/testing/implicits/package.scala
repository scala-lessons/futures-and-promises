package com.testing

package object implicits {

  implicit def strToInt(str: String): Int =  str.toIntOption.getOrElse(0)

  implicit class OprsWrapper[A](a: Ops[A]) {
    def returnA: A = a.value
  }

}

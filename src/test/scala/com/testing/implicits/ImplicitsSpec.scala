package com.testing.implicits

import org.scalatestplus.play.PlaySpec

class ImplicitsSpec extends PlaySpec {

  "Implicits experiments" should {

    "simple case: implicit val" in {

      val engine = new MyMathOps[Int] {

        override def plus(a: Int, b: Int): Int = a + b

        override def plusC(a: Int)(implicit b: Int): Int = plus(a, b)

        override def multiply(a: Int, b: Int): Int = a * b

        override def multiplyC(a: Int)(implicit b: Int): Int = multiply(a, b)
      }

      val a = 1
      implicit val b: Int = 2

      engine.plus(a, b) mustBe engine.plusC(a)

      engine.multiply(a, b) mustBe engine.multiplyC(a)

    }

    "with type class" in {
      val engine = new MyMathOps[Ops[Int]] {

        override def plus(a: Ops[Int], b: Ops[Int]): Ops[Int] = new Ops[Int] {
          override def value: Int = a.value + b.value
        }

        override def plusC(a: Ops[Int])(implicit b: Ops[Int]): Ops[Int] = plus(a, b)

        override def multiply(a: Ops[Int], b: Ops[Int]): Ops[Int] = ???

        override def multiplyC(a: Ops[Int])(implicit b: Ops[Int]): Ops[Int] = ???
      }

      val a = new Ops[Int] {
        override def value: Int = 1
      }

      engine.plusC(a).value mustBe Ops(2).value

    }

    "implicit conversion" in {
      val engine = new MyMathOps[Int] {

        override def plus(a: Int, b: Int): Int = a + b

        override def plusC(a: Int)(implicit b: Int): Int = plus(a, b)

        override def multiply(a: Int, b: Int): Int = a * b

        override def multiplyC(a: Int)(implicit b: Int): Int = multiply(a, b)
      }

      implicit val b: Int = 2

      engine.plusC("23") mustBe 25
    }

    "implicit class" in {
      Ops(2).returnA mustBe 2
    }

  }

}

trait Ops[A] {

  def value: A
}

object Ops {

  implicit val ops: Ops[Int] = new Ops[Int] {
    override def value: Int = 1
  }

  def apply[A](a: A): Ops[A] = new Ops[A] {
    override def value: A = a
  }
}


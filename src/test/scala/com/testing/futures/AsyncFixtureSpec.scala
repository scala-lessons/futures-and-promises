package com.testing.futures

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Second, Seconds, Span}
import org.scalatestplus.play.PlaySpec

import java.util.concurrent.TimeUnit
import scala.concurrent.Await
import scala.concurrent.duration.{FiniteDuration, SECONDS}

class AsyncFixtureSpec extends PlaySpec with ScalaFutures {

  override implicit def patienceConfig: PatienceConfig = PatienceConfig(timeout = Span(10, Seconds), interval = scaled(Span(15, Millis)))

  import scala.concurrent.ExecutionContext.Implicits.global

  "Futures testing specification" should {

    "with promise (parallel)" in {
      val first = AsyncLibrary.printViaPromise(1, FiniteDuration(4, TimeUnit.SECONDS))
      val second = AsyncLibrary.printViaPromise("Not integer", FiniteDuration(0, TimeUnit.SECONDS))

      whenReady(
        for {
          _ <- first
          _ <- second
        } yield {
          ()
        }
      ) {
        _ =>
          succeed
      }
    }

    "with promise" in {
      whenReady(
        for {
          _ <- AsyncLibrary.printViaPromise(1, FiniteDuration(1, TimeUnit.SECONDS))
          _ <- AsyncLibrary.printViaPromise("Not integer", FiniteDuration(1, TimeUnit.SECONDS))
        } yield {
          ()
        }
      ) {
        _ =>
          succeed
      }
    }

    "experiment (parallel)" in {
      val first = AsyncLibrary.printWithDelay("With delay", FiniteDuration(1, TimeUnit.SECONDS))
      val second = AsyncLibrary.print("Without delay")

      whenReady(
        for {
          _ <- first
          _ <- second
        } yield {
          ()
        }
      ) {
        _ =>
          succeed
      }

    }

    "experiment (sequential, case 1)" in {

      whenReady(
        for {
          _ <- AsyncLibrary.print("Without delay")
          _ <- AsyncLibrary.printWithDelay("With delay", FiniteDuration(1, TimeUnit.SECONDS))
        } yield {
          ()
        }
      ) {
        _ =>
          succeed
      }
    }

    "experiment (sequential, case 2)" in {

      whenReady(
        for {
          _ <- AsyncLibrary.printWithDelay("With delay", FiniteDuration(1, TimeUnit.SECONDS))
          _ <- AsyncLibrary.print("Without delay")
        } yield {
          ()
        }
      ) {
        _ =>
          succeed
      }
    }

    "multiply x2" in {

      val arg: Int = 3

      whenReady(
        AsyncLibrary.fm2(arg)
      ) {
        result =>
          result mustBe 6
      }

    }

    "multiply x2 (negative)" in {

      val arg: Int = 2

      whenReady(
        AsyncLibrary.fm2(arg)
      ) {
        result =>
          result must not be 6
      }
    }

    "multiply x2 (await)" in {
      val arg: Int = 3
      val result: Int = Await.result(AsyncLibrary.fm2(arg), FiniteDuration(1, TimeUnit.SECONDS))
      result mustBe 6
    }

    "multiply x6" in {
      val arg: Int = 2

      whenReady(
        AsyncLibrary.fm6(arg)
      ) {
        result =>
          result mustBe 12.0
      }
    }

    "multiply x6m" in {
      val arg: Int = 2

      whenReady(
        AsyncLibrary.fm6fm(arg)
      ) {
        result =>
          result mustBe 12.0
      }
    }

  }

}
